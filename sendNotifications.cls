public with sharing class sendNotifications {

    @AuraEnabled
    public static Boolean sendNotificationstoUsers(String title,String body){

        // to set the landing page when they click on the notification.
        String announcementPage = '{"type": "standard__navItemPage","attributes": {"apiName": "Account"}}';
        // Get the Id for our custom notification type
        CustomNotificationType notificationType = [SELECT Id, DeveloperName 
                                                    FROM CustomNotificationType 
                                                    WHERE DeveloperName='Announcements_Notification'];
                                            
        // Create a new custom notification
        Messaging.CustomNotification notification = new Messaging.CustomNotification();

        // Set the contents for the notification
        notification.setTitle(title);
        notification.setBody(body);

        // Set the notification type and target
        notification.setNotificationTypeId(notificationType.Id);
        notification.settargetPageRef(announcementPage);

        List<String> addressee = new List<String>();
        List<List<String>> addresseeLists = new List<List<String>>();
        
        Integer Count = 0;
        Integer restrictedCount = 0;
        List<User> userList = [ SELECT Id,IsActive  FROM User WHERE IsActive = true];
        for(User usr : userList)
        {
            addressee.add(usr.Id);
            count++;
            restrictedCount++;
            if(restrictedCount == 499){
                restrictedCount = 0;
                addresseeLists.add(addressee);
                addressee = new List<String>();
            }
            if(count == userList.size()){ 
                restrictedCount = 0;
                addresseeLists.add(addressee);
                addressee = new List<String>();
            }
        }
        

        // Actually send the notification
        try {
            for(List<String> strList : addresseeLists){ 
                Set<String> str = new Set<String>();
                str.addAll(strList);
                notification.send(str);
            }
            
            return true;
        }
        catch (Exception e) {
            System.debug('Problem sending notification: ' + e.getMessage());
            return false;
        }
        
    }
}