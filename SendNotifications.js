import {
	LightningElement,
	track
} from 'lwc';
import {
  showToast,
  isValid
} from 'c/pubsub';
import { CloseActionScreenEvent } from 'lightning/actions';
import sendNotificationstoUsers from "@salesforce/apex/sendNotifications.sendNotificationstoUsers";


export default class SendNotifications extends LightningElement {
  @track showSpinner = false;
  @track title;
  @track body;
  @track isRequired = true;

  

  handleInputChange(event) {
          if(event.target.name == 'title'){
            this.title =  event.target.value;
          }

          if(event.target.name == 'body'){
            this.body =  event.target.value;
          }
  }

  closeAction(){
    this.dispatchEvent(new CustomEvent('close'));
    this.title = '';
    this.body = '';
  }

  sendNotification(){

        this.showSpinner = true;

        let isAllValid3 = [
          ...this.template.querySelectorAll("lightning-textarea")
        ].reduce((validSoFar, input) => {
          input.reportValidity();
          return validSoFar && input.checkValidity();
        }, true);

        if (!isValid(this) || !isAllValid3) {
          this.showSpinner = false;
          return;
        }

        sendNotificationstoUsers({
				"title": this.title,
				"body": this.body
        })
          .then(result=>{
                if(result == true){
                    this.dispatchEvent(showToast('', 'Notifications have been successfully send to all users.', 'success'));
                }else{
                    this.dispatchEvent(showToast('', 'Failed to send notifications to Users.', 'error'));
                }
                this.dispatchEvent(new CustomEvent('close'));
                this.title = '';
                this.body = '';
                this.showSpinner = false;
        })
        .catch(error=>{
          this.showSpinner = false;
          this.title = '';
          this.body = '';
          let message = error.message || error.body.message;
          this.dispatchEvent(showToast("", message, "error"));
        });
  }
}